CC = mpicc
BLAS_DIR = /opt/OpenBLAS
BLAS_LIB = $(BLAS_DIR)/lib/libopenblas.a

SRC_DIR  = ./src
INC_DIR  = ./inc $(BLAS_DIR)/include
BLD_DIR  = ./build

ASYNC_SOURCE  = $(SRC_DIR)/ring_asynchronous.c
SYNC_SOURCE   = $(SRC_DIR)/ring_synchronous.c
SOURCES      := $(shell find $(SRC_DIR) -type f -name "*.c" -and -not -name "ring_*.c")

ifeq ($(word 1,$(MAKECMDGOALS)),)
    $(error 'Did not set rule. Type make <clean/build/debug>')
endif

ifeq ($(TARGET), ASYNC)
    SOURCES += $(ASYNC_SOURCE)
else
    ifeq ($(TARGET), SYNC)
        SOURCES += $(SYNC_SOURCE)
    else
        ifneq ($(word 1,$(MAKECMDGOALS)), clean)
            $(error 'TARGET was not set, or not recognized. Acceptable values [SYNC, ASYNC]')
        endif
    endif
endif

OBJECTS   := $(SOURCES:$(SRC_DIR)%.c=$(BLD_DIR)%.o)

MPI_COMPILE_FLAGS := $(shell $(CC) --showme:compile)
MPI_LINK_FLAGS    := $(shell $(CC) --showme:link)

INC_FLAGS  := $(addprefix -I, $(INC_DIR))
LINK_FLAGS := $(BLAS_LIB) $(MPI_LINK_FLAGS) -lm
COMP_FLAGS := $(MPI_COMPILE_FLAGS) -D TIMING


.PHONY: build
build: OPT_FLAG = -O3 -D NDEBUG
build: | clean createDirectory $(OBJECTS)
	$(CC) $(OPT_FLAG) $(OBJECTS) -o $(BLD_DIR)/out.x $(LINK_FLAGS)

.PHONY: debug
debug: OPT_FLAG = -g3
debug: | clean createDirectory $(OBJECTS)
	$(CC) $(OPT_FLAG) $(OBJECTS) -o $(BLD_DIR)/out.x $(LINK_FLAGS)

$(BLD_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(OPT_FLAG) -Wall -c $^ -o $@ $(INC_FLAGS) $(COMP_FLAGS)

.PHONY: createDirectory
createDirectory:
	mkdir -p $(BLD_DIR)

.PHONY: clean
clean:
	rm -rf $(BLD_DIR)
