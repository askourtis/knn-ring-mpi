/*!
 * \file    algorithms.h
 * \brief   Useful function declarations
 *
 * \author  Skourtis Antonis
 * \date    25-11-2019
 */
#ifndef ALGORITHMS_H_GUARD
#define ALGORITHMS_H_GUARD

#include "knnring.h"

//! Compute the euclidian distance matrix [m-by-n]
/*!
 *  \param  out     The result array            [m-by-n]
 *  \param  X       Corpus data points          [n-by-d]
 *  \param  Y       Query data points           [m-by-d]
 *  \param  n       Number of data points       [scalar]
 *  \param  m       Number of query points      [scalar]
 *  \param  d       Number of dimensions        [scalar]
 */
void calculateDistanceArray(double* out, const double* X, const double* Y, int n, int m, int d);

//! Selects the k smallest elements of an array
/*!
 *  \param  out       The k smallest elements of the array
 *  \param  out_idx   An array of coresponding indicies
 *  \param  arr       The array to select from
 *  \param  size      The size of the array
 *  \param  k         Number of smallest elements
 */
void kSelect(double* out, int* out_idx, double* arr, int size, int k);

//! Applies an offset element-wise to an array
/*!
 *  \param  idx     The array
 *  \param  size    The size of the array
 *  \param  offset  The offset to apply
 */
void offset(int* arr, int size, int offset);

//! Merges two kNN-Results to one, while preserving the k-Closest property. Also frees up memory when possible
/*!
 *  \param  res     Reference to the current total results
 *  \param  tmp     The temp results to merge to current
 */
void merge_and_free(knnresult* res, knnresult tmp);
#endif
//-----------EOF-----------//
