/*!
 * \file    defines.h
 * \brief   Defines useful macros
 *
 * \author  Skourtis Antonis
 * \date    27-11-2019
 */
#ifndef DEFINES_H_GUARD
#define DEFINES_H_GUARD
#include <stdio.h>
//------General------//
//Converts a 2D index to an integer, given a dimension size
#define IDX(i,j,d)   ((i)*(d)+(j))
//Calculates the max between 2 numbers
#define MAX(a,b)     (((a)<(b))?(b):(a))
//Swaps 2 values
#define SWAP(a,b)    do {\
                        typeof(a) _temp=(a);\
                        a=(b);\
                        b=_temp;\
                     } while(0)

//------EXCEPTION HANDLING------//
//Terminates the process printing an error message M and returning an error code E
#define TERMINATE(E,M)  do {fprintf(stderr, "%s:%d Termination Reason: %s\n", __FILE__, __LINE__, M); exit(E);} while(0)
//Memory Error Code
#define MEM_ERR     1

//------DEBUG MODE------//
#ifdef NDEBUG
//Does nothing
#define DEBUG_LOG(...) ((void)0)
#else
//Prints to the console
#define DEBUG_LOG(...) fprintf(stdout, __VA_ARGS__)
#endif

//------TIMING MODE------//
#ifndef TIMING
//Executes a block of code, returning -1
#define TIMED_BLOCK(B) ({{B} -1.0;})
//Does nothing
#define REPORT_TIME(N,T) ((void)0)
#else
#include <sys/time.h>
//Runs a block of code, tracking the time it took to complete execution
#define TIMED_BLOCK(B) ({\
                           struct timeval _start_stopwatch_, _end_stopwatch_;\
                           gettimeofday(&_start_stopwatch_, (void*)0);\
                           {B}\
                           gettimeofday(&_end_stopwatch_, (void*)0);\
                           ((_end_stopwatch_.tv_sec-_start_stopwatch_.tv_sec)+((_end_stopwatch_.tv_usec-_start_stopwatch_.tv_usec))/1000000.0);\
                        })
//Report the time
#define REPORT_TIME(N,T) fprintf(stdout, "Execution time of [%s] is: %lf\n", N, T)
#endif

//------MPI HELPERS------//
//Master Rank
#define MPI_MASTER_RANK     0
//Cycle Data Tag
#define MPI_CYCLE_TAG       0

//Qualifies if rank is a priority sender
#define IS_PRIORITY_SENDER(r) ((r) % 2)
//Mathematical mod
#define MOD(a,m)  ((((a) % (m)) + (m)) % (m))

#endif
//-----------EOF-----------//
