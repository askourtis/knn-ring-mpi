/*!
 * \file    algorithms.c
 * \brief   Defines all of the declarations in algorithms.h
 *
 * \author  Skourtis Antonis
 * \date    25-11-2019
 */
#include "algorithms.h"
#include "defines.h"

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <cblas.h>

//------Helper Declarations------//
//Max-Heapify
static void heapify(double* arr, int* idx, int size, int i);
//Sink last element to the correct place, assuming the rest array is sorted
static void sink(double* arr, int* idx, int size);

//-------Definitions-------//
void calculateDistanceArray(double* const out, const double* const X, const double* const Y, const int n, const int m, const int d) {
    assert(out!=NULL);
    assert(X!=NULL);
    assert(Y!=NULL);

    //Set first row in out array = Xi dot Xi (for all i)
    for(int i=0; i<n; ++i) {
        out[IDX(0,i,n)] = 0;
        for(int j=0; j<d; ++j)
            out[IDX(0,i,n)] += X[IDX(i,j,d)]*X[IDX(i,j,d)];
    }

    //Set each row in out array equal to the first row
    for(int i=1; i<m; ++i)
        memcpy(out+(IDX(i,0,n)), out+(IDX(0,0,n)), n*sizeof(double));

    //Add to each row in out array Yi dot Yi
    for(int i=0; i<m; ++i) {
        double dot = 0;
        for(int j=0; j<d; ++j)
            dot += Y[IDX(i,j,d)]*Y[IDX(i,j,d)];

        for(int j=0; j<n; ++j)
            out[IDX(i,j,n)] += dot;
    }

    //out += -2(Y•X^T)
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, m, n, d, -2.0, Y, d, X, d, 1.0, out, n);
    //No need for sqrt just yet. (sqrt is a purely increasing function, so greater-equal-lower checks will not change after the function application)
}

void kSelect(double* const out, int* const out_idx, double* const arr, const int size, const int k) {
    assert(out!=NULL);
    assert(out_idx!=NULL);
    assert(arr!=NULL);
    assert(k <= size);


    //Assume k first elements are the smallest
    if(out != arr)
        memmove(out, arr, k*sizeof(double));

    for(int i=0; i<k; ++i)
        out_idx[i] = i;

    //Build max heap
    for(int i=k/2-1; i>=0; --i)
        heapify(out, out_idx, k, i);

    //Replace max of the heap with any smaller element and re-heap
    for(int i=k; i<size; ++i) {
        if(out[0] > arr[i]) {
            out[0] = arr[i];
            out_idx[0] = i;
            heapify(out, out_idx, k, 0);
        }
    }

    //Sort the array
    for(int i=0; i<k; ++i) {
        SWAP(out[0], out[k-i-1]);
        SWAP(out_idx[0], out_idx[k-i-1]);
        heapify(out, out_idx, k-i-1, 0);
    }
}

void offset(int* arr, int size, int offset) {
    assert(arr!=NULL);
    for(int i=0; i<size; ++i)
        arr[i]+=offset;
}

void merge_and_free(knnresult* const res, knnresult tmp) {
    assert(res!=NULL);
    assert(tmp.ndist!=NULL);
    assert(tmp.nidx!=NULL);
    assert(res->m == tmp.m);
    assert(res->k == tmp.k);

    const int m = res->m;
    const int k = res->k;

    //If res is not set yet. Set it to tmp
    if(res->ndist == NULL) {
        res->ndist = tmp.ndist;
        res->nidx = tmp.nidx;
        return;
    }

    //For all points
    for(int i=0; i<m; ++i) {
        //Select correct arrays from res and tmp
        double* const rdist = res->ndist+(IDX(i,0,k));
        int*    const ridx  = res->nidx+(IDX(i,0,k));

        double* const tdist = tmp.ndist+(IDX(i,0,k));
        int*    const tidx  = tmp.nidx+(IDX(i,0,k));

        //While the max element of result distances is larger than the temp distances
        for(int j=0; j<k && rdist[k-1] > tdist[j]; ++j) {
            //Replace the max with the current element of the tmp array
            rdist[k-1] = tdist[j];
            ridx[k-1] = tidx[j];
            //Sink last element to the correct place
            sink(rdist, ridx, k);
        }
    }

    free(tmp.ndist);
    free(tmp.nidx);
}

//-------Helper Definitions-------//
static void heapify(double* const arr, int* const idx, const int size, const int i) {
    assert(arr!=NULL);
    assert(idx!=NULL);
    int largest = i;
    const int l = 2*i + 1;
    const int r = 2*i + 2;

    if (l < size && arr[l] > arr[largest])
        largest = l;

    if (r < size && arr[r] > arr[largest])
        largest = r;

    if (largest != i) {
        SWAP(arr[i], arr[largest]);
        SWAP(idx[i], idx[largest]);
        heapify(arr, idx, size, largest);
    }
}

static void sink(double* arr, int* idx, int size) {
    //While ith element is smaller than (i-1)th swap them
    for(int i=size-1; i>0 && arr[i] < arr[i-1]; --i) {
        SWAP(arr[i], arr[i-1]);
        SWAP(idx[i], idx[i-1]);
    }
}
//-----------EOF-----------//
