/*!
 * \file    knn.c
 * \brief   Implements kNN definition of knnring.h
 *
 * \author  Skourtis Antonis
 * \date    18-11-2019
 */
#include "knnring.h"
#include "defines.h"
#include "algorithms.h"

#include <cblas.h>
#include <math.h>

#include <assert.h>
#include <stdlib.h>

//-------Definitions-------//
knnresult kNN(double* const X, double* const Y, const int n, const int m, const int d, const int k) {
    double* const dist  = malloc(m*n*sizeof(double));
    int*    const nidx  = malloc(m*k*sizeof(int));
    if(dist == NULL)
        TERMINATE(MEM_ERR, "Could not allocate dist.");
    if(nidx == NULL)
        TERMINATE(MEM_ERR, "Could not allocate nidx.");
    calculateDistanceArray(dist, X, Y, n, m, d);
    //If corpus is query then the diagonal of the euclidean distance matrix is always zero
    if(X == Y)
        for(int i=0; i<m; ++i)
            dist[IDX(i,i,m)] = 0.0;
    //Select k closest neighbors
    for(int i=0; i<m; ++i)
        kSelect(dist+(IDX(i,0,k)), nidx+(IDX(i,0,k)), dist+(IDX(i,0,n)), n, k);
    //Free the excess memory
    double* const ndist = realloc(dist, m*k*sizeof(double));
    if(ndist == NULL)
        TERMINATE(MEM_ERR, "Could not reallocate dist.");
    //Apply sqrt to all elements of the array
    //A distance close to zero can actually be a negative zero, thus the sqrt will return nan.
    //Setting all nans to zero is a way to avoid confusion
    for(int i=0; i<m*k; ++i)
        ndist[i] = (ndist[i]>0)?(sqrt(ndist[i])):(0);
    return (knnresult){.ndist=ndist, .nidx=nidx, .m=m, .k=k};
}
//-----------EOF-----------//
