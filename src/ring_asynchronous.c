/*!
 * \file    ring_synchronous.c
 * \brief   Implements, via a synchronous approach, distrAllkNN definition of knnring.h
 *
 * \author  Skourtis Antonis
 * \date    23-11-2019
 */
#include "knnring.h"
#include "defines.h"
#include "algorithms.h"

#include <stdlib.h>
#include <mpi.h>
#include <assert.h>
#include <math.h>

//-------Struct Definitions-------//
typedef struct indexed_element {
    int index;
    double value;
} IndexedElement;

//------Helper Declarations------//
//Reduces Indexed Elements (MAX)
static void operationMaxReduceIndexedElement(IndexedElement* in, IndexedElement* inout, int* len, MPI_Datatype* datatype);
//Reduces Indexed Elements (MIN)
static void operationMinReduceIndexedElement(IndexedElement* in, IndexedElement* inout, int* len, MPI_Datatype* datatype);
//Creates and commits the IndexedElement Type
static void createAndCommitIndexedArrayType(MPI_Datatype* out);

//-------Definitions-------//
knnresult distrAllkNN(double* const X, const int n, const int d, const int k) {
    assert(X!=NULL);
    //Create the custom type
    MPI_Datatype MPI_INDEXED_ELEMENT;
    createAndCommitIndexedArrayType(&MPI_INDEXED_ELEMENT);
    //Create the costume reductions
    MPI_Op MPI_IE_MAX, MPI_IE_MIN;
    MPI_Op_create(operationMaxReduceIndexedElement, 1, &MPI_IE_MAX);
    MPI_Op_create(operationMinReduceIndexedElement, 1, &MPI_IE_MIN);

    //Process rank and count
    int w_rank, w_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &w_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &w_size);
    //Corpus array
    double* Y = malloc(n*d*sizeof(double));
    if(Y==NULL)
        TERMINATE(MEM_ERR, "Could not allocate Y");

    //Buffer for the incoming points
    double* Z = malloc(n*d*sizeof(double));
    if(Z==NULL)
        TERMINATE(MEM_ERR, "Could not allocate Z");

    knnresult res = (knnresult){.ndist=NULL, .nidx=NULL, .m=n, .k=k};
    knnresult tmp;

    //Total computation time
    double total_comp_time = 0.0;

    //Time the whole loop
    //Total execution time
    double total_time = TIMED_BLOCK(
        //Cycle workload
        for(int iteration=0; iteration<w_size-1; ++iteration) {
            MPI_Request req[2];
            DEBUG_LOG("PROCESS %d: Sending to %d\n", w_rank, MOD(w_rank+1, w_size));
            MPI_Isend(iteration?Y:X, n*d, MPI_DOUBLE, MOD(w_rank+1, w_size), MPI_CYCLE_TAG, MPI_COMM_WORLD, req+(0));
            DEBUG_LOG("PROCESS %d: Expecting from %d\n", w_rank, MOD(w_rank-1, w_size));
            MPI_Irecv(Z, n*d, MPI_DOUBLE, MOD(w_rank-1, w_size), MPI_CYCLE_TAG, MPI_COMM_WORLD, req+(1));
            //Perform kNN on Y and X
            DEBUG_LOG("PROCESS %d: Performing knn (Iteration: %d)\n", w_rank, iteration);
            total_comp_time += TIMED_BLOCK(
                tmp = kNN(iteration?Y:X, X, n, n, d, k);
                //Apply an offset to the indices
                offset(tmp.nidx, tmp.m*tmp.k, MOD(w_rank-1-iteration, w_size)*n);
                //Merge the results to res
                merge_and_free(&res, tmp);
                //Free up memory
            );
            MPI_Waitall(2, req, MPI_STATUS_IGNORE);
            SWAP(Y, Z);
        }
    );

    DEBUG_LOG("PROCESS %d: Performing knn (Iteration: %d)\n", w_rank, w_size-1);
    {
        const double tmp_comp_time = TIMED_BLOCK(
            tmp = kNN(Y, X, n, n, d, k);
            offset(tmp.nidx, tmp.m*tmp.k, MOD(w_rank, w_size)*n);
            merge_and_free(&res, tmp);
        );
        total_time      += tmp_comp_time;
        total_comp_time += tmp_comp_time;
    }

    IndexedElement local_max  = (IndexedElement){.index=-1, .value=-INFINITY},
                   local_min  = (IndexedElement){.index=-1, .value=+INFINITY},
                   global_max = (IndexedElement){.index=-1, .value=-INFINITY},
                   global_min = (IndexedElement){.index=-1, .value=+INFINITY};

    //Find local max and min
    for (int i=0; i<n; ++i) {
        if(local_max.value < res.ndist[IDX(i,k-1,k)]) {
            local_max.value = res.ndist[IDX(i,k-1,k)];
            local_max.index = res.nidx[IDX(i,k-1,k)];
        }

        if(local_min.value > res.ndist[IDX(i,0,k)]) {
            local_min.value = res.ndist[IDX(i,0,k)];
            local_min.index = res.nidx[IDX(i,0,k)];
        }
    }

    //Global reduction
    MPI_Reduce(&local_max, &global_max, 1, MPI_INDEXED_ELEMENT, MPI_IE_MAX, MPI_MASTER_RANK, MPI_COMM_WORLD);
    MPI_Reduce(&local_min, &global_min, 1, MPI_INDEXED_ELEMENT, MPI_IE_MIN, MPI_MASTER_RANK, MPI_COMM_WORLD);

    if(w_rank == MPI_MASTER_RANK) {
        printf("Global max is %lf with index %d\n", global_max.value, global_max.index);
        printf("Global min is %lf with index %d\n", global_min.value, global_min.index);
        REPORT_TIME("Total", total_time);
        REPORT_TIME("Computations", total_comp_time);
        REPORT_TIME("Communications", total_time - total_comp_time);
    }

    //Cleanup
    free(Z);
    free(Y);
    MPI_Op_free(&MPI_IE_MAX);
    MPI_Op_free(&MPI_IE_MIN);
    MPI_Type_free(&MPI_INDEXED_ELEMENT);
    return res;
}

//-------Helper Definitions-------//
static void operationMaxReduceIndexedElement(IndexedElement* in, IndexedElement* inout, int* len, MPI_Datatype* datatype) {
    for(int i=0; i<*len; ++i) {
        if(in[i].value > inout[i].value) {
            inout[i].value = in[i].value;
            inout[i].index = in[i].index;
        }
    }
}

static void operationMinReduceIndexedElement(IndexedElement* in, IndexedElement* inout, int* len, MPI_Datatype* datatype) {
    for(int i=0; i<*len; ++i) {
        if(in[i].value < inout[i].value) {
            inout[i].value = in[i].value;
            inout[i].index = in[i].index;
        }
    }
}

static void createAndCommitIndexedArrayType(MPI_Datatype* out) {
    const int nitems=2;
    const int blocklengths[2] = {1,1};
    const MPI_Datatype types[2] = {MPI_INT, MPI_DOUBLE};
    const MPI_Aint offsets[2] = {offsetof(IndexedElement, index), offsetof(IndexedElement, value)};
    MPI_Type_create_struct(nitems, blocklengths, offsets, types, out);
    MPI_Type_commit(out);

}
//-----------EOF-----------//
