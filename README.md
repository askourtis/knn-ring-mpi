# KNN MPI

A school project

Implementing in MPI a distributed search algorithm for the 𝑘 nearest neighbors (𝑘-NN) of each point 𝑥 ∈ 𝑋.

## How to build

Find the BLAS directory in the given system, and set the **BLAS_DIR** variable in the Makefile. Also make sure that the **BLAS_LIB** variable points to the correct binary

Add a source code file containing the main() function in the ./src/ directory.

Then execute the following:

<pre>make &lt;build/debug&gt; TARGET=&lt;SYNC/ASYNC&gt;</pre>

Example
<pre>make debug TARGET=SYNC</pre>
*Compiles the Synchronous implementation in debug mode*

Executable will be '**BUILD_DIR**/out.x'. Default is 'build/out.x'

**Note:** Only the mpi object file changes, the rest remain the same.

## Troubleshooting

If getting error '*TARGET was not set, or not recognized. Acceptable values [SYNC, ASYNC]*'. The **TARGET** variable was not set correctly. Please consult the mentioned example.

Please feel free to send me a mail, if there are any issues. *Mail address given in the pdf report.*
